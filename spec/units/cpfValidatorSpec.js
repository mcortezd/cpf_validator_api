const app = require("../../models/Cpf");

describe("cpf validator", function() {
  const validCpf = "61837223246";
  const invalidCpf1 = "613372232402";
  const invalidCpf2 = "618372232402";
  const sameDigitsCpf = "00000000000";

  describe("when cpf is valid", function() {
    it("check tenth digit", function() {
      const result = app.validateDigit(validCpf, 9);

      expect(result).toBe(true);
    });

    it("check eleventh digit", function() {
      const result = app.validateDigit(validCpf, 10);

      expect(result).toBe(true);
    });

    it("quantity of digits is valid", function() {
      const result = app.checkCorrectSize("12345678901");

      expect(result).toBe(true);
    });
  });

  describe("when cpf is invalid", function() {
    it("check tenth digit invalid", function() {
      const result = app.validateDigit(invalidCpf1, 9);

      expect(result).toBe(false);
    });

    it("check eleventh digit invalid", function() {
      const result = app.validateDigit(invalidCpf2, 10);

      expect(result).toBe(false);
    });

    it("check all digits are same", function() {
      const result = app.checkAllSameDigits(sameDigitsCpf);

      expect(result).toBe(true);
    });

    it("check all digits are same with other numbers", function() {
      const result = app.checkAllSameDigits("11111111111");

      expect(result).toBe(true);
    });

    it("quantity of digits is invalid", function() {
      const result = app.checkCorrectSize("123456789");

      expect(result).toBe(false);
    });
    it("wrong format", function() {
      const result = app.checkCorrectSize("123.123.123-30");

      expect(result).toBe(false);
    });
  });
});
